# Use Case Open pump

## Contained in

* [Finishing](../tasks/11_finishing.md), as Open pump, step 1

## Steps

| ID | name | arm | digital_twin | operation_type |
| -- | ---- | --- | ------------ | -------------- |
| 1 |_press pump button_ | 2 |[POSE(pump_head)](../subsubtasks/digital_twin/dt01_pose.md) [REGION(pump_head)](../subsubtasks/digital_twin/dt03_region.md) | manipulation |

