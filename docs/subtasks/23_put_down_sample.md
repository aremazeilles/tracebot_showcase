# Use Case Put down sample

## Contained in

* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 7
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 13
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 19
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 25
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 31
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 37
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 43
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 49
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 55
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 61
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 67
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 73
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 79
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 85
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 91
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 97
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 103
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 109
* [Sample transferring](../tasks/06_sample_transferring.md), as Put down sample, step 115

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[Locate sample tray](../subsubtasks/vision/v00_locate_object.md) | | [sample tray](../object/sample.md) | vision |
| 2 |[Place sample](../subsubtasks/manipulation/m02_place.md) | 1 | [sample](../object/sample.md) | manipulation |

