# Use Case Attach cut tube to canister air vent

## Contained in

* [Cutting and filling](../tasks/10_cutting_and_closing.md), as Attach cut tube to canister air vent, step 4

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[Locate canister](../subsubtasks/vision/v00_locate_object.md) | |[COMPONENT_REGION(tube_joints)](../subsubtasks/digital_twin/dt04_component_region.md) [REGION(tube)](../subsubtasks/digital_twin/dt03_region.md) [REGION_COLOR_IMAGE(tube)](../subsubtasks/digital_twin/dt06_region_color_image.md) | [canister](../object/canister.md) | vision |
| 2 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 3 |[Grab](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [tube](../object/tube.md) | manipulation |
| 4 |[Insert tube into air vent](../subsubtasks/manipulation/m15_insert_tube_into_air_vent.md) | 1 | | | manipulation |
| 5 |[Locate canister](../subsubtasks/vision/v00_locate_object.md) | |[COMPONENT_REGION(tube_joints)](../subsubtasks/digital_twin/dt04_component_region.md) [REGION(tube)](../subsubtasks/digital_twin/dt03_region.md) [REGION_COLOR_IMAGE(tube)](../subsubtasks/digital_twin/dt06_region_color_image.md) | [canister](../object/canister.md) | vision |
| 6 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 7 |[Grab](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [tube](../object/tube.md) | manipulation |
| 8 |[Insert tube into air vent](../subsubtasks/manipulation/m15_insert_tube_into_air_vent.md) | 1 | | | manipulation |

