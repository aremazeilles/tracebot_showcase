# Use Case Attach red plugs

## Contained in

* [Wetting](../tasks/05_wetting.md), as Attach red plugs, step 7
* [Sample Filtering](../tasks/07_sample_filtering.md), as Attach red plugs, step 1
* [washing](../tasks/08_washing.md), as Attach red plugs, step 2

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[Locate canister](../subsubtasks/vision/v00_locate_object.md) | | | [canister](../object/canister.md) | vision |
| 2 |[Locate plug](../subsubtasks/vision/v00_locate_object.md) | | | [red plug](../object/red_plug.md) | vision |
| 3 |[Grab red plug](../subsubtasks/manipulation/m01_grab.md) | 1 |[REGION_COLOR_IMAGE(top, canister)](../subsubtasks/digital_twin/dt06_region_color_image.md) [POSE(red plug)](../subsubtasks/digital_twin/dt01_pose.md) | [red plug](../object/red_plug.md) | manipulation |
| 4 |[Move red plug over canister air vent](../subsubtasks/manipulation/m04_move.md) | 1 | | [red plug](../object/red_plug.md) | manipulation |
| 5 |[Insert red plug into air vent](../subsubtasks/manipulation/m08_insert_red_plug_into_air_vent.md) | 2 |[VISUAL_EXPECTATION_MATCH(canister)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | | manipulation |

