# Use Case Detach red plugs

## Contained in

* [Wetting](../tasks/05_wetting.md), as Detach red plugs, step 11
* [Sample Filtering](../tasks/07_sample_filtering.md), as Detach red plugs, step 5
* [washing](../tasks/08_washing.md), as Detach red plugs, step 4

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[Locate plug](../subsubtasks/vision/v00_locate_object.md) | | | [red plug](../object/red_plug.md) | vision |
| 2 |[Grab red plug](../subsubtasks/manipulation/m01_grab.md) | 1 |[REGION_COLOR_IMAGE(top, canister)](../subsubtasks/digital_twin/dt06_region_color_image.md) [POSE(red_cap)](../subsubtasks/digital_twin/dt01_pose.md) | [red plug](../object/red_plug.md) | manipulation |
| 3 |[Pull red plug](../subsubtasks/manipulation/m03_pull.md) | 1 | | [red plug](../object/red_plug.md) | manipulation |
| 4 |[Place red plug](../subsubtasks/manipulation/m02_place.md) | 1 |[VISUAL_EXPECTATION_MATCH(canister)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | [red plug](../object/red_plug.md) | manipulation |

