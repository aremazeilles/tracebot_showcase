# Use Case Close Clamp valve

## Description

Closing a tube is realized by closing the clamps attached to it.

## Contained in

* [Media Filling](../tasks/09_media_filling.md), as Close tube, step 5
  with object = [white sample clamp](../object/clamp.md)
* [Media Filling](../tasks/09_media_filling.md), as Close tube, step 12
  with object = [red sample clamp](../object/clamp.md)
* [Cutting and filling](../tasks/10_cutting_and_closing.md), as Close tube at first canister, step 1
  with object = [red canister clamp](../object/clamp.md)
* [Cutting and filling](../tasks/10_cutting_and_closing.md), as Close tube at second canister, step 2
  with object = [white canister clamp](../object/clamp.md)

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[locate tube clamp valve](../subsubtasks/vision/v00_locate_object.md) | | _object_ | vision |
| 2 |[Close clamp valve](../subsubtasks/manipulation/m13_close_clamp.md) | 1 | | manipulation |

