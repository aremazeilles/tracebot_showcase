# Use Case Setup pump

## Contained in

* [Kit mounting](../tasks/03_kit_mounting.md), as Setup pump, step 4

## Steps

| ID | name | object | operation_type |
| -- | ---- | ------ | -------------- |
| 1 |[locate pump knob](../subsubtasks/vision/v00_locate_object.md) | [pump knob](../object/pump.md) | vision |
| 2 |[locate pump buttons](../subsubtasks/vision/v00_locate_object.md) | [pump buttons](../object/pump.md) | vision |
| 3 |_setup pump_ | | manipulation |
| 3.1 |_read settings from gui_ | | vision |
| 3.2 |_turn pump knob_ | | manipulation |
| 3.3 |_push pump button_ | | manipulation |

