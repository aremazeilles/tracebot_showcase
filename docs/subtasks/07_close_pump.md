# Use Case Close pump

## Contained in

* [Kit mounting](../tasks/03_kit_mounting.md), as Close pump, step 3

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate close pump button](../subsubtasks/vision/v00_locate_object.md) | | | [close pump button](../object/pump.md) | vision |
| 2 |_press close pump button_ | 1 |[POSE(pump_head)](../subsubtasks/digital_twin/dt01_pose.md) [REGION(pump_head)](../subsubtasks/digital_twin/dt03_region.md) | | manipulation |

