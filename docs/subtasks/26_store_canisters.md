# Use Case Store canisters

## Contained in

* [Finishing](../tasks/11_finishing.md), as Store canisters, step 6

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[Locate canister](../subsubtasks/vision/v00_locate_object.md) | | | [canister](../object/canister.md) | vision |
| 2 |[Grab  canister](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [canister](../object/canister.md) | manipulation |
| 3 |[Detach canister](../subsubtasks/manipulation/m11_detach_canister.md) | 1 |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) | [canister](../object/canister.md) | manipulation |
| 4 |[Place canister](../subsubtasks/manipulation/m02_place.md) | 1 | | [canister](../object/canister.md) | manipulation |
| 5 |[Locate canister](../subsubtasks/vision/v00_locate_object.md) | | | [canister](../object/canister.md) | vision |
| 6 |[Grab  canister](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [canister](../object/canister.md) | manipulation |
| 7 |[Detach canister](../subsubtasks/manipulation/m11_detach_canister.md) | 1 |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) | [canister](../object/canister.md) | manipulation |
| 8 |[Place canister](../subsubtasks/manipulation/m02_place.md) | 1 | | [canister](../object/canister.md) | manipulation |

