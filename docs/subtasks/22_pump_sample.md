# Use Case Pump sample

## Contained in

* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 5
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 11
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 17
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 23
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 29
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 35
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 41
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 47
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 53
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 59
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 65
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 71
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 77
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 83
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 89
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 95
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 101
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 107
* [Sample transferring](../tasks/06_sample_transferring.md), as Pump sample, step 113

## Steps

| ID | name | arm | operation_type |
| -- | ---- | --- | -------------- |
| 1 |[Pump sample](../subsubtasks/manipulation/m10_pump_sample.md) | [1, 2] | manipulation |

