# Use Case Remove tube from pump

## Contained in

* [Finishing](../tasks/11_finishing.md), as Remove tube from pump, step 2

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |_Undefined_ | |[COMPONENT_REGION(tube_joints)](../subsubtasks/digital_twin/dt04_component_region.md) [REGION(tube)](../subsubtasks/digital_twin/dt03_region.md) [REGION_COLOR_IMAGE(tube)](../subsubtasks/digital_twin/dt06_region_color_image.md) | | |
| 2 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 3 |[Grab tube](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [tube](../object/tube.md) | manipulation |
| 4 |[Grab tube](../subsubtasks/manipulation/m01_grab.md) | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [tube](../object/tube.md) | manipulation |
| 5 |[Slide tube out of pump head](../subsubtasks/manipulation/m16_slide_tube_out_of_pump_head.md) | [1, 2] | | | manipulation |

