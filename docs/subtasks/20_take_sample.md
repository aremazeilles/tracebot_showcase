# Use Case Take sample

## Contained in

* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 2
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 8
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 14
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 20
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 26
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 32
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 38
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 44
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 50
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 56
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 62
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 68
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 74
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 80
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 86
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 92
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 98
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 104
* [Sample transferring](../tasks/06_sample_transferring.md), as Take sample, step 110

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[Locate sample tray](../subsubtasks/vision/v00_locate_object.md) | | [sample tray](../object/sample.md) | vision |
| 2 |[Grab](../subsubtasks/manipulation/m01_grab.md) | 1 | [sample](../object/sample.md) | manipulation |
| 3 |[Pull](../subsubtasks/manipulation/m03_pull.md) | 1 | [sample](../object/sample.md) | manipulation |

