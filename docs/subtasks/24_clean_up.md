# Use Case Clean up

## Contained in

* [Sample transferring](../tasks/06_sample_transferring.md), as Clean up, step 117
* [Finishing](../tasks/11_finishing.md), as Clean up, step 4
* [Finishing](../tasks/11_finishing.md), as Clean up, step 5

## Steps

| ID | name | object | operation_type |
| -- | ---- | ------ | -------------- |
| 1 |[Locate bottle](../subsubtasks/vision/v00_locate_object.md) | [bottle](../object/bottle.md) | vision |
| 2 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | [tube](../object/tube.md) | vision |
| 3 |[Locate pack](../subsubtasks/vision/v00_locate_object.md) | [steri test package](../object/steri_test_package.md) | vision |
| 4 |[Locate sample tray](../subsubtasks/vision/v00_locate_object.md) | [sample tray](../object/sample.md) | vision |

