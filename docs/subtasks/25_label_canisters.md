# Use Case Label Canisters

## Contained in

* [Sample Filtering](../tasks/07_sample_filtering.md), as LabelCanisters, step 3

## Steps

| ID | name | object | operation_type |
| -- | ---- | ------ | -------------- |
| 1 |[Locate canister](../subsubtasks/vision/v00_locate_object.md) | [canister](../object/canister.md) | vision |
| 2 |[Locate pen](../subsubtasks/vision/v00_locate_object.md) | [pen](../object/pen.md) | vision |

