# Use Case Place petri dish

## Contained in

* [manual_preparation](../tasks/01_manual_preparation.md), as Place petri dish, step 2

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[locate petri dish](../subsubtasks/vision/v00_locate_object.md) | | [petri dish](../object/petri_dish.md) | vision |
| 2 |_open petri dish_ | ['Arm 1 (+ 2)', 'Arm 1 force feedback'] | | manipulation |
| 3 |_place and cover petri dish_ | [1, 2] | | manipulation |
| 3.1 |[place](../subsubtasks/manipulation/m02_place.md) | 1 | [petri dish](../object/petri_dish.md) | manipulation |
| 3.2 |_cover petri dish_ | [2] | | manipulation |

