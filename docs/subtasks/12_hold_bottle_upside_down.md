# Use Case Hold bottle upside down

## Contained in

* [Wetting](../tasks/05_wetting.md), as Hold bottle upside down, step 2
* [Move bottle into holder](36_move_bottle_to_holder.md), as Grab bottle, step 1

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate bottle](../subsubtasks/vision/v00_locate_object.md) | | | [bottle](../object/bottle.md) | vision |
| 2 |[grab](../subsubtasks/manipulation/m01_grab.md) | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [bottle](../object/bottle.md) | manipulation |
| 3 |[move (rotate)](../subsubtasks/manipulation/m04_move.md) | 2 |[POSE(bottle)](../subsubtasks/digital_twin/dt01_pose.md) | [bottle](../object/bottle.md) | manipulation |

