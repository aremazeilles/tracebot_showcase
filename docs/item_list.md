# Item list

Listing of all elements of the environment referenced.

| Name | image |
| ---- | ---- |
| [needle](object/needle.md) | ![needle](images/objects/needle.jpg) |
| [pump](object/pump.md) | ![pump](images/objects/pump.png) |
| [red plug](object/red_plug.md) | ![red plug](images/objects/plug_red.jpg) |
| [canister_outlet_port](object/canister_outlet_port.md) | ![canister_outlet_port](images/objects/canister_outlet_port.jpg) |
| [bottle](object/bottle.md) | ![bottle](images/objects/bottle_big.jpg) |
| [sample](object/sample.md) | |
| [steri test package](object/steri_test_package.md) | ![steri test package](images/objects/steri_test_package.jpg) |
| [pen](object/pen.md) | |
| [robot](object/robot.md) | |
| [canister](object/canister.md) | ![canister](images/objects/canister.jpg) |
| [petri dish](object/petri_dish.md) | |
| [bottle holder](object/bottle_holder.md) | ![bottle holder](images/objects/bottle_holder.png) |
| [steri test kit](object/steri_test_kit.md) | ![steri test kit](images/objects/steri_test_kit.jpg) |
| [vial](object/vial.md) | |
| [yellow plug](object/yellow_plug.md) | ![yellow plug](images/objects/plug_yellow.jpg) |
| [scissors](object/scissors.md) | |
| [septum](object/septum.md) | ![septum](images/objects/stopper.png) |
| [canister air vent](object/canister_air_vent.md) | ![canister air vent](images/objects/canister_air_vent.jpg) |
| [clamp](object/clamp.md) | ![clamp](images/objects/clamps.jpg) |
| [tube](object/tube.md) | ![tube](images/objects/tube.jpg) |
| [drain tray](object/drain_tray.md) | ![drain tray](images/objects/drain_tray.jpg) |
| [pump head](object/pump_head.md) | ![pump head](images/objects/pump_head.png) |
