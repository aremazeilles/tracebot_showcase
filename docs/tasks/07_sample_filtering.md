# Use Case Sample Filtering

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/07_sample_filtering_rssl.mp4)

<video controls>
  <source src="../videos/07_sample_filtering_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Sample filtering, step 7

## Steps

| ID | name | time_abs | time_rel |
| -- | ---- | -------- | -------- |
| 1 |[Attach red plugs](../subtasks/17_attach_red_plugs.md) | [9:50, 10:02] | [0:00, 0:12] |
| 2 |[Start pump](../subtasks/11_start_pump.md) | [10:02, 10:04] | [0:12, 0:14] |
| 3 |[LabelCanisters](../subtasks/25_label_canisters.md) | [10:04, 10:13] | [0:14, 0:23] |
| 4 |[Wait](../subtasks/13_wait.md) | [10:04, 10:45] | [0:14, 0:55] |
| 5 |[Detach red plugs](../subtasks/18_detach_red_plugs.md) | [10:45, 10:50] | [0:55, 1:00] |

## Additional videos
