# Use Case Kit unpacking

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/02_kit_unpacking_rssl.mp4)

<video controls>
  <source src="../videos/02_kit_unpacking_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Kit unpacking, step 2

## Steps

| ID | name | time_abs | time_rel |
| -- | ---- | -------- | -------- |
| 1 |[Open kit](../subtasks/03_open_kit.md) | [4:11, 4:16] | [0:00, 0:05] |
| 2 |[Unpack kit](../subtasks/04_unpack_kit.md) | [4:16, 4:19] | [0:05, 0:08] |

## Additional videos
