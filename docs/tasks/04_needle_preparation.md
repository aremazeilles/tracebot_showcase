# Use Case NeedlePreparation

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/04_needle_preparation_rssl.mp4)

<video controls>
  <source src="../videos/04_needle_preparation_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Needle preparation, step 4

## Steps

| ID | name | object | time_abs | time_rel |
| -- | ---- | ------ | -------- | -------- |
| 1 |[Remove needle cap](../subtasks/09_remove_needle_cap.md) | | [4:41, 4:44] | [0:00, 0:03] |
| 2 |[Insert needle](../subtasks/10_insert_needle.md) | [washing bottle](../object/bottle.md) | [4:44, 4:49] | [0:03, 0:08] |

## Additional videos
