# Use Case Finishing

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/11_finishing_rssl.mp4)

<video controls>
  <source src="../videos/11_finishing_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Finishing, step 11

## Steps

| ID | name | time_abs | time_rel |
| -- | ---- | -------- | -------- |
| 1 |[Open pump](../subtasks/32_open_pump.md) | [15:41, 15:47] | [0:00, 0:06] |
| 2 |[Remove tube from pump](../subtasks/33_remove_tube_from_pump.md) | [15:45, 15:47] | [0:04, 0:06] |
| 3 |[Take bottle down](../subtasks/14_take_bottle_down.md) | [15:47, 15:48] | [0:06, 0:07] |
| 4 |[Clean up](../subtasks/24_clean_up.md) | [15:48, 16:06] | [0:07, 0:25] |
| 5 |[Clean up](../subtasks/24_clean_up.md) | [16:06, 16:14] | [0:25, 0:33] |
| 6 |[Store canisters](../subtasks/26_store_canisters.md) | [16:14, 16:21] | [0:33, 0:40] |

## Additional videos
