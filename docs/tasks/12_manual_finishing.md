# Use Case Manual Finishing

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/12_manual_finishing_rssl.mp4)

<video controls>
  <source src="../videos/12_manual_finishing_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Manual finishing, step 12

## Steps

| ID | name | time_abs | time_rel |
| -- | ---- | -------- | -------- |
| 1 |[Clean](../subtasks/34_clean.md) | [16:21, 17:22] | [0:00, 1:01] |
| 2 |[Move out petri dish](../subtasks/35_move_out_petri_dish.md) | [17:22, 18:28] | [1:01, 2:07] |

## Additional videos
