# Use Case washing

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/08_washing_rssl.mp4)

<video controls>
  <source src="../videos/08_washing_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Washing, step 8

## Steps

| ID | name | time_abs | time_rel |
| -- | ---- | -------- | -------- |
| 1 |[Move bottle into holder](../subtasks/36_move_bottle_to_holder.md) | [10:50, 10:53] | [0:00, 0:03] |
| 2 |[Attach red plugs](../subtasks/17_attach_red_plugs.md) | [11:09, 11:14] | [0:19, 0:24] |
| 3 |[Stop pump](../subtasks/15_stop_pump.md) | [12:22, 12:24] | [1:32, 1:34] |
| 4 |[Detach red plugs](../subtasks/18_detach_red_plugs.md) | [12:24, 12:30] | [1:34, 1:40] |

## Additional videos

## Additional information

3 steps were initially defined here at the end of the sequence, but the video cut required to move them to next step
