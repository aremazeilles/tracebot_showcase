# Use Case Cutting and filling

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/10_cutting_and_closing_rssl.mp4)

<video controls>
  <source src="../videos/10_cutting_and_closing_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Cutting and closing, step 10

## Steps

| ID | name | object | time_abs | time_rel |
| -- | ---- | ------ | -------- | -------- |
| 1 |[Close tube at first canister](../subtasks/28_close_clamp_valve.md) | [red canister clamp](../object/clamp.md) | [15:12, 15:16] | [0:00, 0:04] |
| 2 |[Close tube at second canister](../subtasks/28_close_clamp_valve.md) | [white canister clamp](../object/clamp.md) | [15:16, 15:19] | [0:04, 0:07] |
| 3 |[Cut tubes](../subtasks/30_cut_tubes.md) | | [15:19, 15:29] | [0:07, 0:17] |
| 4 |[Attach cut tube to canister air vent](../subtasks/31_attach_cut_tube_to_canister_air_vent.md) | | [15:29, 15:41] | [0:17, 0:29] |

## Additional videos
