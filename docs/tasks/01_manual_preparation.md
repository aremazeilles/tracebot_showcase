# Use Case manual_preparation

<!-- to not see it when compiled with pandoc, see: https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output/ -->

[//]: # ![type:video](../videos/01_manual_preparation_rssl.mp4)

<video controls>
  <source src="../videos/01_manual_preparation_rssl.mp4" type="video/mp4">
</video>
<br/>

## Contained in

* [Sterility Testing](../root_index.md), as Manual Preparation, step 1

## Steps

| ID | name | time_abs | time_rel |
| -- | ---- | -------- | -------- |
| 1 |[Label petri dish](../subtasks/01_label_petri_dish.md) | [3:35, 4:06] | [0:00, 0:31] |
| 2 |[Place petri dish](../subtasks/02_place_petri_dish.md) | [4:06, 4:11] | [0:31, 0:36] |

## Additional videos
