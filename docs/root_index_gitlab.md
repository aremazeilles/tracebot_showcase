# Use Case Sterility Testing


## Steps

| ID | name | video |
| -- | ---- | ----- |
| 1 |[Manual Preparation](tasks/01_manual_preparation.md) | ![type:video](videos/01_manual_preparation_rssl.mp4) |
| 2 |[Kit unpacking](tasks/02_kit_unpacking.md) | ![type:video](videos/02_kit_unpacking_rssl.mp4) |
| 3 |[Kit mounting](tasks/03_kit_mounting.md) | ![type:video](videos/03_kit_mounting_rssl.mp4) |
| 4 |[Needle preparation](tasks/04_needle_preparation.md) | ![type:video](videos/04_needle_preparation_rssl.mp4) |
| 5 |[Wetting](tasks/05_wetting.md) | ![type:video](videos/05_wetting_rssl.mp4) |
| 6 |[Sample transferring](tasks/06_sample_transferring.md) | ![type:video](videos/06_sample_transferring_rssl.mp4) |
| 7 |[Sample filtering](tasks/07_sample_filtering.md) | ![type:video](videos/07_sample_filtering_rssl.mp4) |
| 8 |[Washing](tasks/08_washing.md) | ![type:video](videos/08_washing_rssl.mp4) |
| 9 |[Media filling](tasks/09_media_filling.md) | ![type:video](videos/09_media_filling_rssl.mp4) |
| 10 |[Cutting and closing](tasks/10_cutting_and_closing.md) | ![type:video](videos/10_cutting_and_closing_rssl.mp4) |
| 11 |[Finishing](tasks/11_finishing.md) | ![type:video](videos/11_finishing_rssl.mp4) |
| 12 |[Manual finishing](tasks/12_manual_finishing.md) | ![type:video](videos/12_manual_finishing_rssl.mp4) |

