# Use Case Sterility Testing


## Steps

| ID | name | video |
| -- | ---- | ----- |
| 1 |[Manual Preparation](tasks/01_manual_preparation.md) | <video controls> <source src="videos/01_manual_preparation_rssl.mp4" type="video/mp4"> </video> |
| 2 |[Kit unpacking](tasks/02_kit_unpacking.md) | <video controls> <source src="videos/02_kit_unpacking_rssl.mp4" type="video/mp4"> </video> |
| 3 |[Kit mounting](tasks/03_kit_mounting.md) | <video controls> <source src="videos/03_kit_mounting_rssl.mp4" type="video/mp4"> </video> |
| 4 |[Needle preparation](tasks/04_needle_preparation.md) | <video controls> <source src="videos/04_needle_preparation_rssl.mp4" type="video/mp4"> </video> |
| 5 |[Wetting](tasks/05_wetting.md) | <video controls> <source src="videos/05_wetting_rssl.mp4" type="video/mp4"> </video> |
| 6 |[Sample transferring](tasks/06_sample_transferring.md) | <video controls> <source src="videos/06_sample_transferring_rssl.mp4" type="video/mp4"> </video> |
| 7 |[Sample filtering](tasks/07_sample_filtering.md) | <video controls> <source src="videos/07_sample_filtering_rssl.mp4" type="video/mp4"> </video> |
| 8 |[Washing](tasks/08_washing.md) | <video controls> <source src="videos/08_washing_rssl.mp4" type="video/mp4"> </video> |
| 9 |[Media filling](tasks/09_media_filling.md) | <video controls> <source src="videos/09_media_filling_rssl.mp4" type="video/mp4"> </video> |
| 10 |[Cutting and closing](tasks/10_cutting_and_closing.md) | <video controls> <source src="videos/10_cutting_and_closing_rssl.mp4" type="video/mp4"> </video> |
| 11 |[Finishing](tasks/11_finishing.md) | <video controls> <source src="videos/11_finishing_rssl.mp4" type="video/mp4"> </video> |
| 12 |[Manual finishing](tasks/12_manual_finishing.md) | <video controls> <source src="videos/12_manual_finishing_rssl.mp4" type="video/mp4"> </video> |

