# Skill Detach canister

## Contained in

* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Detach canister, step 4
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Detach canister, step 13
* [Store canisters](../../subtasks/26_store_canisters.md), as Detach canister, step 3
* [Store canisters](../../subtasks/26_store_canisters.md), as Detach canister, step 7

## Description

Similar to the pull action used elsewhere, but may require some wiggling.
