# Skill Close clamp

## Contained in

* [Close Clamp valve](../../subtasks/28_close_clamp_valve.md), as Close clamp valve, step 2

## Description

Press on the clamp valve in the right spot until it 'clicks'.
Assumes the clamp pose is known.
