# Skill Close clamp valve

## Contained in

* [Close tube](../../subtasks/28_close_tube.md), as Close clamp valve, step 2

## Description

Press on the clamp valve in the right spot until it 'clicks'.
