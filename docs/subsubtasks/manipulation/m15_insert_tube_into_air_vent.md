# Skill Insert tube into air vent

## Contained in

* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Insert tube into air vent, step 4
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Insert tube into air vent, step 8

## Description

Insert tube end into the canister's air vent.
