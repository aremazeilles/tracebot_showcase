# Skill Insert needle into bottle

## Contained in

* [Insert needle](../../subtasks/10_insert_needle.md), as insert needle into object, step 4

## Description

Inserts the needle into the rubber patch in the bottle cap.

* TBD evaluate whether a position-driven control is enough or whether we need to do explicit force control
