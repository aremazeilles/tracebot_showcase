# Skill Pull _object_

* Required information: object

## Contained in

* [Open kit](../../subtasks/03_open_kit.md), as pull, step 6
  with object = [pack corner](../../object/steri_test_package.md)
* [Unpack kit](../../subtasks/04_unpack_kit.md), as pull pack and tube out of pack, step 5
  with object = [tube](../../object/tube.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as Pull needle cap, step 5
  with object = [needle cap](../../object/needle.md)
* [Open plug bag](../../subtasks/16_open_plug_bag.md), as Pull plug bag, step 4
  with object = [plug bag](../../object/red_plug.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Pull red plug, step 3
  with object = [red plug](../../object/red_plug.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Pull needle, step 6
  with object = [needle](../../object/needle.md)
* [Take sample](../../subtasks/20_take_sample.md), as Pull, step 3
  with object = [sample](../../object/sample.md)

## Description

Applies a motion in a given direction with a given force until _some condition_ is met (e.g. sudden force drop, displacement reached).
