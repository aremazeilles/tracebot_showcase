# Skill Open clamp valve

## Contained in

* [Open tube](../../subtasks/29_open_tube.md), as Open clamp valve, step 2

## Description

Press and slide finger on the clamp valve to release the lock.
