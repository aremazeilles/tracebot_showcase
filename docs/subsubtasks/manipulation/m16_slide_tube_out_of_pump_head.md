# Skill Slide tube out of pump head

## Contained in

* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Slide tube out of pump head, step 5

## Description

Similar to the [Pull](m03_pull.md) subsubtask, but dual-arm.
