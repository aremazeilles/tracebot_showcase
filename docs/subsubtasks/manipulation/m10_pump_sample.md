# Skill Pump sample

## Contained in

* [Pump sample](../../subtasks/22_pump_sample.md), as Pump sample, step 1

## Description

Maintain the needle tip in contact with liquid by slowly rotating the vial to bring the sample to its top.
