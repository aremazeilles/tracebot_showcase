# Skill VISUAL_EXPECTATION_MATCH

* Required information: object

## Contained in

* [fit canister 1 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Grab tube and move tube to temporary position out of the way, step 3.3_3
  with object = [canister 1](../../object/canister.md)
* [fit canister 2 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Grab Tube and move tube to temporary position out of the way, step 7.3_3
  with object = [canister 2](../../object/canister.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as Grab needle cap, step 4_3
  with object = [needle](../../object/needle.md)
* [Insert needle](../../subtasks/10_insert_needle.md), as insert needle into object, step 4_3
  with object = [needle](../../object/needle.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Insert red plug into air vent, step 5_1
  with object = [canister](../../object/canister.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Place red plug, step 4_1
  with object = [canister](../../object/canister.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Pull needle, step 6_1
  with object = [bottle](../../object/bottle.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 8_3
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 11_3
  with object = [drain tray](../../object/drain_tray.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 17_3
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 20_3
  with object = [drain tray](../../object/drain_tray.md)

## Description

Get a visual match classification result that compares the rendered expectation of the scene and the real world percept.

1. After running the ideal process in the digital twin, compare the visual sensory stream from the real world with the one from the digital twin for verification and validation
2. After the ongoing process has been rendered in the digital twin, compare the visual sensory stream from the real world with the one from the digital twin for verification and validation
3. After an action is terminated in the real and virtual worlds, compare the final visual images of both worlds for verification and validation.
