# Skill REGION_COLOR_IMAGE

* Required information: object

## Contained in

* [fit canister 1 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Grab tube and move tube to temporary position out of the way, step 3.3_2
  with object = [canister 1](../../object/canister.md)
* [fit canister 2 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Grab Tube and move tube to temporary position out of the way, step 7.3_2
  with object = [canister 2](../../object/canister.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as Grab needle cap, step 4_1
  with object = [needle](../../object/needle.md)
* [Insert needle](../../subtasks/10_insert_needle.md), as insert needle into object, step 4_1
  with object = [needle](../../object/needle.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Grab red plug, step 3_1
  with object = [canister](../../object/canister.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Grab red plug, step 2_1
  with object = [canister](../../object/canister.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Grab needle, step 5_1
  with object = [bottle](../../object/bottle.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 8_2
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 11_2
  with object = [drain tray](../../object/drain_tray.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 17_2
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 20_2
  with object = [drain tray](../../object/drain_tray.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 1_3
  with object = [tube](../../object/tube.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 5_3
  with object = [tube](../../object/tube.md)
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Undefined, step 1_3
  with object = [tube](../../object/tube.md)

## Description

Get a ROI in synthetic color image of the estimated belief.
Simular to REGION_COLOR_IMAGE, but for specific regions in the scene.

1. How does the detected canister look like when it is rendered in the digital twin?
